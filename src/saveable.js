
window.addEventListener("DOMContentLoaded", function () {
    // http://blog.garstasio.com/you-dont-need-jquery/ajax/
    function $post(url, data, success) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', url);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            
        xhr.onload = function() {
            console.log(xhr.status);
            if (xhr.status === 200) {
                // var response = JSON.parse(xhr.responseText);
                var response = xhr.responseText;
                success.call(xhr, response)
            }
        };
        //xhr.send(JSON.stringify(data));
        xhr.send(data);
    }

    function save () {
        // http://stackoverflow.com/questions/6088972/get-doctype-of-an-html-as-string-with-javascript
        var node = document.doctype,
            html = "";
        if (node) {
            html = "<!DOCTYPE "
                 + node.name
                 + (node.publicId ? ' PUBLIC "' + node.publicId + '"' : '')
                 + (!node.publicId && node.systemId ? ' SYSTEM' : '') 
                 + (node.systemId ? ' "' + node.systemId + '"' : '')
                 + ">\n";
        }
        html += document.documentElement.outerHTML;
        $post(this.window.location, "text="+encodeURIComponent(html), function (data) {
            console.log("saved", data);
        }); 
    }

    // console.log("ready");
    document.addEventListener("keypress", function (e) {
        // console.log("keypress", e);
        if (e.ctrlKey && e.key.toLowerCase() == "s") {
            e.preventDefault();
            // e.stopPropagation();
            // console.log("save");
            save();
        }
        if (e.ctrlKey && e.key.toLowerCase() == "e") {
            e.preventDefault();
            // TOGGLE CONTENTEDITABLE
            var cv = document.body.getAttribute("contenteditable");
            if (cv === null || cv === undefined) {
                document.body.setAttribute("contenteditable", "");
                console.log("contenteditable enabled");
            } else {
                document.body.removeAttribute("contenteditable");
                console.log("contenteditable disabled");
            }
        }
        // var request = new XMLHttpRequest();
        // request.open('POST', '/my/url', true);
        // request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        // request.send(data);

    })


})
